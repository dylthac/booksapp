from tkinter import CASCADE
from django.db import models

# Create your models here.
class Book(models.Model):
    title = models.CharField(max_length=100, unique=True)
    authors = models.ManyToManyField("Author", related_name="books")
    pages = models.SmallIntegerField(null=True)
    ISBN = models.BigIntegerField(null=True)
    image = models.URLField(null=True, blank=True)
    print = models.BooleanField(null=True)
    published = models.SmallIntegerField(null=True)
    description = models.TextField(null=True)

    def __str__(self):
        return self.title + " by " + str(self.authors.first())

class  Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


class  BookReview(models.Model):
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()



class Genre(models.Model):
    genre_name = models.CharField(max_length=20)
    
    def __str__(self):
        return self.genre_name

class Magazine(models.Model):
    title = models.CharField(max_length=50)
    release = models.CharField(max_length=25, blank=False)
    description = models.TextField(null=True)
    cover = models.URLField(null=True, blank=True)
    genres = models.ManyToManyField("Genre", related_name='magazines')
    # page = models.ImageField(null=True)
    # issued = models.ManyToManyField("Issue", related_name ="magazine")
  
    
    
    def __str__(self):
        return self.title 

class Issue(models.Model):
    magazine = models.ForeignKey(Magazine, related_name="issues", on_delete=models.CASCADE, null=True)
    issue_title = models.CharField(max_length=200, unique=True, blank=True)
    date_published = models.SmallIntegerField(null=True, blank=True)
    page_count = models.SmallIntegerField(null=True,blank=True)
    issue_number = models.SmallIntegerField(null=True, blank=True)
    cover_issue_image = models.URLField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.issue_title


