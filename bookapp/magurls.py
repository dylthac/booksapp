from django.contrib import admin
from django.urls import path, include
from bookapp.views import show_genre, show_mag, create_mag, show_mag_details, edit_mag, delete_mag

urlpatterns = [
    path('', show_mag, name="show_mags"),
    path('create/', create_mag, name="create_mag"),
    path('<int:pk>/', show_mag_details, name ="show_mag_details"),
    path('<int:pk>/edit', edit_mag, name="edit_mag"),
    path('<int:pk>/delete', delete_mag, name= "delete_mag"),
    path('genre/', show_genre, name= "show_genre"),

]