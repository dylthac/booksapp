from django.contrib import admin
from django.urls import path, include
from bookapp.views import delete_book, edit_book, show_book, create_book, show_book_details


urlpatterns = [
    path('create/', create_book, name="create_book"),
    path('', show_book, name = "show_book"),
    path('<int:pk>/', show_book_details, name= "show_book_details"),
    path('<int:pk>/edit', edit_book, name = "edit_book"),
    path('<int:pk>/delete', delete_book, name = "delete_book"),
    
    

]
