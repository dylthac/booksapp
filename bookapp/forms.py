from django import forms
from bookapp.models import Book, Magazine

class BookForm(forms.ModelForm):

    class Meta:
        model = Book
        
        fields = [
            "title",
            "authors",
            "pages",
            "ISBN",
            "image",
            "print",
            "published",
            "description",
        ]

class MagazineForm(forms.ModelForm):

    class Meta:
        model = Magazine
        fields = [
            "title",
            "release",
            "description",
            "cover",
        ]