from django.contrib import admin
from bookapp.models import Author, Book, BookReview, Genre, Issue, Magazine

# Register your models here.
class BookAdmin(admin.ModelAdmin):
    pass

admin.site.register(Book, BookAdmin)
admin.site.register(Magazine)
admin.site.register(BookReview)
admin.site.register(Author)
admin.site.register(Issue)
admin.site.register(Genre)