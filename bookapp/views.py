from __future__ import generator_stop
from lib2to3.pgen2.tokenize import generate_tokens
from multiprocessing import context
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from bookapp.models import Book, Magazine, Genre, Issue
from bookapp.forms import BookForm, MagazineForm
from django.views.generic.detail import DetailView
# Create your views here.

def show_book(request):
    books = Book.objects.all()
    context = {
        "books": books
    }
    return render(request, "books/list.html", context)



def create_book(request):
    context= {}
    form = BookForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect("show_book")

    context['form'] = form
    return render(request, "books/create.html", context)

#class BookDetailView(DetailView):
#    model = Book
    
def show_book_details(request, pk):
    book = Book.objects.get(pk=pk)
    context= {
        "book": book
    }
    return render(request, "books/Book_details.html", context)
    

def edit_book(request, pk):
    context = {}
    obj = get_object_or_404(Book, pk=pk)
    form = BookForm(request.POST or None, instance = obj)
    if form.is_valid():
        book = form.save()
        return redirect("show_book")
    context["form"] = form
    return render(request, "books/book_update.html", context)

def delete_book(request, pk):
    context = {}
    obj = get_object_or_404(Book, pk=pk)
    if request.method == "POST":
        obj.delete()
        return redirect("show_book")
    return render(request, "books/delete.html", context)


def show_mag(request):
    magazines = Magazine.objects.all()
    context = {
        "magazines" : magazines
    }
    return render(request, "magazines/mag_list.html", context)

def create_mag(request):
    context= {}
    form = MagazineForm(request.POST or None)
    if form.is_valid():
        magazine = form.save()
        return redirect("show_mags")
    
    context['form'] = form
    return render (request, "magazines/mag_create.html", context)

def show_mag_details(request, pk):
    magazine = Magazine.objects.get(pk=pk)
    context= {
        "magazine": magazine
    }
    return render (request, "magazines/mag_detail.html", context)

def edit_mag(request,pk):
    context = {}
    obj = get_object_or_404(Magazine, pk=pk)
    form = MagazineForm(request.POST or None, instance = obj)
    if form.is_valid():
        magazine = form.save()
        return redirect("show_mags")
    context["form"] = form
    return render(request, "magazines/mag_update.html", context)

def delete_mag(request, pk):
    context = {}
    obj = get_object_or_404(Magazine, pk=pk)
    if request.method == "POST":
        obj.delete()
        return redirect("show_mags")
    return render(request, "magazines/mag_delete.html", context)

def show_genre(request):
    genres = Genre.objects.all()
    context = {
        "genres": genres

    }
    return render(request, "magazines/genre_view.html", context)

